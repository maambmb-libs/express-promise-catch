module.exports = function( fn ) {

    return function( req, res, next ) {

        Promise.resolve()
            .then( () => fn( req, res, next ) )
            .catch( e => next( e ) );

    };

};
